package com.example.home.colorchange2;

import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Handler handler;

    private List<Integer> colors = new ArrayList<>();
    private int count = 0;
    RelativeLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainLayout = findViewById(R.id.main_layout);

        colors.add(Color.RED);
        colors.add(Color.BLUE);
        colors.add(Color.BLACK);
        colors.add(Color.GREEN);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                mainLayout.setBackgroundColor(colors.get(msg.what));
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (count == 0) {
                        while (count < colors.size()) {
                            handler.sendEmptyMessage(count);
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            count++;
                        }
                    } else {
                        count--;
                        handler.sendEmptyMessage(count);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }

                }
            }
        }).start();


    }
}
